import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Album } from 'src/app/shared/album/album.model';
import { AlbumService } from 'src/app/shared/album/album.service';

@Component({
  selector: 'app-highest-rated-expanded',
  templateUrl: './highest-rated-expanded.component.html',
  styleUrls: ['./highest-rated-expanded.component.css']
})
export class HighestRatedExpandedComponent implements OnInit, OnDestroy {

  constructor(private albumService: AlbumService) { }
  
  albumsRated: Album[];
  subscription: Subscription;

  ngOnInit() {
    this.subscription = this.albumService.albumsChanged
      .subscribe(
        (albumsRated: Album[]) => {
          this.albumsRated = albumsRated;
        }
      );
    this.albumsRated = this.albumService.getHighestRatedAlbums();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
