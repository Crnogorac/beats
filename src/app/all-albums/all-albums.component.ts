import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Album } from '../shared/album/album.model';
import { AlbumService } from '../shared/album/album.service';

@Component({
  selector: 'app-all-albums',
  templateUrl: './all-albums.component.html',
  styleUrls: ['./all-albums.component.css']
})
export class AllAlbumsComponent implements OnInit {

  constructor(private albumService: AlbumService) { }
  
  allAlbums: Album[];
  subscription: Subscription;

  ngOnInit() {
    this.subscription = this.albumService.albumsChanged
      .subscribe(
        (allAlbums: Album[]) => {
          this.allAlbums = allAlbums;
        }
      );
    this.allAlbums = this.albumService.getSortedAlbums();
  }

}
