import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddAlbumComponent } from "./add-album/add-album.component";
import { AdminGuard } from "./add-album/admin.guard";
import { AllAlbumsComponent } from "./all-albums/all-albums.component";
import { AuthComponent } from "./auth/auth.component";
import { AuthGuard } from "./auth/auth.guard";
import { HighestRatedComponent } from "./highest-rated/highest-rated.component";
import { HomeComponent } from "./home/home.component";
import { NewReleasesComponent } from "./new-releases/new-releases.component";
import { AlbumResolverService } from "./shared/album-resolver.service";
import { AlbumDetailHighestComponent } from "./shared/album/album-detail/album-detail-highest/album-detail-highest.component";
import { AlbumDetailNewComponent } from "./shared/album/album-detail/album-detail-new/album-detail-new.component";
import { AlbumDetailUpcomingComponent } from "./shared/album/album-detail/album-detail-upcoming/album-detail-upcoming.component";
import { AlbumDetailComponent } from "./shared/album/album-detail/album-detail.component";
import { AlbumsStartComponent } from "./shared/album/albums-start/albums-start.component";
import { UpcomingAlbumsComponent } from "./upcoming-albums/upcoming-albums.component";

const appRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent,
        canActivate: [AuthGuard],
        resolve: [AlbumResolverService]
    },
    { path: 'highest-rated-albums', component: HighestRatedComponent,
        resolve: [AlbumResolverService],
    children: [
        { path: '', component: AlbumsStartComponent },
        { path: ':id', component: AlbumDetailHighestComponent,
            resolve: [AlbumResolverService] }
    ] },
    { path: 'upcoming-albums', component: UpcomingAlbumsComponent,
        resolve: [AlbumResolverService],
    children: [
        { path: '', component: AlbumsStartComponent },
        { path: ':id', component: AlbumDetailUpcomingComponent,
            resolve: [AlbumResolverService] }
    ] },
    { path: 'new-releases', component: NewReleasesComponent, 
        resolve: [AlbumResolverService],
    children: [
        { path: '', component: AlbumsStartComponent },
        { path: ':id', component: AlbumDetailNewComponent,
            resolve: [AlbumResolverService] }
    ]},
    { path: 'all-albums', component: AllAlbumsComponent,
        resolve: [AlbumResolverService],
    children: [
        { path: '', component: AlbumsStartComponent, },
        { path: ':id', component: AlbumDetailComponent,
            resolve: [AlbumResolverService] }
    ] },
    { path: 'auth', component: AuthComponent },
    { path: 'add-new-album', component: AddAlbumComponent,
        resolve: [AlbumResolverService],
        canActivate: [AdminGuard]}
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}