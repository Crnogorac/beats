import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Album } from "./album/album.model";
import { AlbumService } from "./album/album.service";
import { DataStorageService } from "./data-storage.service";

@Injectable({ providedIn: 'root' })
export class AlbumResolverService implements Resolve<Album[]>{

    constructor(private albumService: AlbumService,
                private dsStorage: DataStorageService){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        let albums: Album[] = this.albumService.getAlbums();
        
        if(albums.length === 0){
            return this.dsStorage.fetchAlbums();
        }
        else {
            return albums;
        }
    }

}