import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, tap } from "rxjs/operators";
import { Album } from "./album/album.model";
import { AlbumService } from "./album/album.service";
import { Ratings } from "./album/rating.model";

@Injectable({
    providedIn: 'root'
})
export class DataStorageService { 

    constructor(private http: HttpClient,
                private albumService: AlbumService){}


    fetchAlbums(){
        return this.http
            .get<Album[]>('https://beats-5369e-default-rtdb.firebaseio.com/albums.json')           
            .pipe(
                    map(albums => {
                         return albums.map(album => {
                            return {...album, 
                                    ratings: album.ratings ? album.ratings: [],
                                    comments: album.comments ? album.comments: []
                                };
                                });
                        }),
                    tap(albums => {
                        this.albumService.setAlbums(albums);
                    })
                )
    }

    

}