import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { DataStorageService } from "../data-storage.service";
import { Album } from "./album.model";
import { AlbumService } from "./album.service";

@Injectable({
    providedIn: 'root'
})
export class AlbumResolverService implements Resolve<Album[]>{

    constructor(private dsService: DataStorageService,
                private albumService: AlbumService){}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        const albums = this.albumService.getAlbums()

        if(albums.length === 0){
            return this.dsService.fetchAlbums();
        }

        else {
            return albums;
        }
        
    }

}