import { Component, Input, OnInit } from '@angular/core';
import { Album } from '../../album.model';
import { AlbumService } from '../../album.service';

@Component({
  selector: 'app-album-item-expanded',
  templateUrl: './album-item-expanded.component.html',
  styleUrls: ['./album-item-expanded.component.css']
})
export class AlbumItemExpandedComponent implements OnInit {
  @Input() album: Album;
  @Input() index: number;

  constructor(public albumService: AlbumService){}

  ngOnInit(){}
  

}
