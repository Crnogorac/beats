import { Component, OnInit } from '@angular/core';
import { Album } from '../album.model';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-album-list-expanded',
  templateUrl: './album-list-expanded.component.html',
  styleUrls: ['./album-list-expanded.component.css']
})
export class AlbumListExpandedComponent implements OnInit {
  
  albums: Album[] = [];

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
    this.albums = this.albumService.getAlbums();
  }

}
