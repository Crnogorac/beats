import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Album } from "./album.model";
import { Ratings } from "./rating.model";
import { Comment } from "./comment.model";


@Injectable({
    providedIn: 'root'
})
export class AlbumService {
    albumsChanged = new Subject<Album[]>();
    private albums: Album[] = [];

    constructor(private http: HttpClient){}
    
    setAlbums(albums: Album[]){
        this.albums = albums;
        this.albumsChanged.next(this.albums.slice());
    }

    getAlbums(){
        return this.albums.slice();
    }

    getSortedAlbums(){
        let albums: Album[] = this.getAlbums();

        albums.sort( (a, b) => 
             a.name > b.name ? 1 : -1
        );

        return albums.slice();
    }

    getOneAlbum(index: number){
        return this.albums[index];
    }
    
    getOneSortedAlbum(index: number){
        let allAlbums: Album[] = this.getSortedAlbums();

        return allAlbums[index];
        
    }

    getAlbumIndex(album: Album){
        return this.getSortedAlbums().indexOf(album);
    }

    getAlbumRating(album: Album){
        let ratingAvg: number = 0;
        
        if(album.ratings.length > 0){
            for(let index = 0; index < album.ratings.length; index++){
                ratingAvg = ratingAvg + album.ratings[index].rating;
            }

            ratingAvg = ratingAvg / album.ratings.length;
            return Math.round(ratingAvg*10)/10;
        }
        return 'Not rated yet';
    }

    getComments(album: Album){

        let allComments: Comment[] = [];
        console.log(allComments);
        
       if(album.comments.length > 0){
        for(let index = 0; index < album.comments.length; index++){
            allComments.push(album.comments[index]);
        }
        return allComments;
       }
    }

 
    getHighestRatedAlbums(){
        let albumsRated: Album[] = [];
        let albumDate: Date;
        let oneAlbum: Album;

        for (let index = 0; index < this.albums.length; index++) {
            if(this.getAlbumRating(this.albums[index]) == 5 ){
                oneAlbum = this.albums[index];
                albumDate = this.albums[index].releaseDate;
                albumsRated.push(this.albums[index]);
            }
          }
          return albumsRated.slice().slice(0, 3);
    }

    getOneHighest(index: number){
        let albumsHighest: Album[] = this.getHighestRatedAlbums();

        return albumsHighest[index];
    }

    getUpcomingAlbums(){
        let albumsUpcoming: Album[] = [];
        let currentDate: Date = new Date();
        let albumDate: Date;

        for(let index = 0; index < this.albums.length; index++){
            albumDate = new Date(this.albums[index].releaseDate)
            if(albumDate > currentDate){
                albumsUpcoming.push(this.albums[index]);
            }
        }

        return albumsUpcoming.slice();
    }

    getOneUpcoming(index: number){
        let albumsUpcoming: Album[] = this.getUpcomingAlbums();

        return albumsUpcoming[index];
    }

    getNewReleases(){
        let albumsNewReleases: Album[] = [];
        let currentDate: Date = new Date();
        let razlika = currentDate.getMonth() - 4;
    	let date: Date;
        let day: number = currentDate.getDate();
        let month: number;
        let year: number;
        let albumDate: Date;

        if(razlika < 0){
            month= 12 + razlika + 1;
            year = currentDate.getFullYear() - 1;
        
        }
        else{
            month = razlika + 1;
            year = currentDate.getFullYear();
        }

        date = new Date(`${year}-${month}-${day}`);

        for(let index = 0; index < this.albums.length; index++){
            albumDate = new Date(this.albums[index].releaseDate)
            if(albumDate >= date && albumDate <= currentDate){
                albumsNewReleases.push(this.albums[index]);
            }
        }
        return albumsNewReleases.slice();
    }

    getOneNewRelease(index: number){
        let albumsNewReleases: Album[] = this.getNewReleases();

        return albumsNewReleases[index]; 
    }

    addRating(album: Album, rating: number){
        const userData: {
            email: string, 
            id: string,
            _token: string,
            _tokenExpDate: string
        } = JSON.parse(localStorage.getItem('userData'));

        let email: string = userData.email;

        let ratings = new Ratings(email, rating);

        album.ratings.push(ratings);

        this.http.put('https://beats-5369e-default-rtdb.firebaseio.com/albums.json',
                       this.getAlbums()).subscribe(response =>
                        console.log(response));
    }

    addComment(album: Album, text: string){
        const userData: {
            email: string, 
            id: string,
            _token: string,
            _tokenExpDate: string
        } = JSON.parse(localStorage.getItem('userData'));

        let email: string = userData.email;

        let comment = new Comment(email, text);

        album.comments.push(comment);

        this.http.put('https://beats-5369e-default-rtdb.firebaseio.com/albums.json',
                       this.getAlbums()).subscribe(response =>
                        console.log(response));
    }

    addAlbum(name: string, artist: string, description: string, releaseDate: Date, imgURL: string, spotify?: string, youtube?: string){

        let newAlbum: Album = new Album(name, artist, imgURL, [], [], releaseDate, description, spotify ? spotify : '', youtube ? youtube: '' );


        this.albums.push(newAlbum);
        this.albumsChanged.next(this.albums.slice());

        this.http.put('https://beats-5369e-default-rtdb.firebaseio.com/albums.json',
                        this.getAlbums()).subscribe(response =>
                        console.log(response));

    }

    checkIfVoted(album: Album){
        const userData: {
            email: string, 
            id: string,
            _token: string,
            _tokenExpDate: string
        } = JSON.parse(localStorage.getItem('userData'));

        let email: string = userData.email;

        for(let index = 0; index < album.ratings.length; index++){
            if(album.ratings[index].mail === email){
                return true;
            }
        }
        return false;
    }

    checkIfAlbumExists(name: string, artist: string){
        
        for(let index = 0; index < this.albums.length; index++){
            if((this.albums[index].name === name) && (this.albums[index].artist === artist)){
                return true;
            }
        }
        return false;
    }

    getUser(){
        const userData: {
            email: string, 
            id: string,
            _token: string,
            _tokenExpDate: string
        } = JSON.parse(localStorage.getItem('userData'));

        let email: string = userData.email;

        return email;
    }

  
}


