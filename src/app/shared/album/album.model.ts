import { Comment } from "./comment.model";
import { Ratings } from "./rating.model";

export class Album {
    public name: string;
    public artist: string;
    public imagePath: string;
    public ratings: Ratings[];
    public comments: Comment[];
    public releaseDate: Date;
    public description: string;
    public linkSpotify: string;
    public linkYoutube: string;

    constructor(name: string, artist: string, imagePath: string, ratings: Ratings[], comments: Comment[], releaseDate: Date, description: string, linkSpotify: string, linkYoutube: string){
        this.name = name;
        this.artist = artist;
        this.imagePath = imagePath;
        this.ratings = ratings;
        this.comments = comments;
        this.releaseDate = releaseDate;
        this.description = description;
        this.linkSpotify = linkSpotify;
        this.linkYoutube = linkYoutube;
    }
}