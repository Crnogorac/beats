import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Album } from '../../album.model';
import { AlbumService } from '../../album.service';

@Component({
  selector: 'app-album-detail-highest',
  templateUrl: './album-detail-highest.component.html',
  styleUrls: ['./album-detail-highest.component.css']
})
export class AlbumDetailHighestComponent implements OnInit {

  album: Album;
  id: number;
  isRated = false;
  text: string;

  constructor(public albumService: AlbumService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.album = this.albumService.getOneHighest(this.id);
          this.isRated = this.albumService.checkIfVoted(this.album);
        }
      );
  }
  onAddRating(album: Album, rating: number){
    this.albumService.addRating(album, rating);
    this.isRated = this.albumService.checkIfVoted(album);
  }

  onAddComment(album: Album, text: string, form: NgForm){
    this.albumService.addComment(album, text);

    form.reset();
  }
}

