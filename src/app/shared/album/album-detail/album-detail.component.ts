import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Album } from '../album.model';
import { AlbumService } from '../album.service';
import { Comment } from '../comment.model';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.css']
})
export class AlbumDetailComponent implements OnInit {
  album: Album;
  id: number;
  isRated = false;
  comments: Comment[];
  text: string;

  constructor(public albumService: AlbumService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.album = this.albumService.getOneSortedAlbum(this.id);
          this.isRated = this.albumService.checkIfVoted(this.album);
        }
      );

      this.comments = this.albumService.getComments(this.album);
  }

  onAddRating(album: Album, rating: number){
    this.albumService.addRating(album, rating);
    this.isRated = this.albumService.checkIfVoted(album);
  }

  onAddComment(album: Album, text: string, form: NgForm){
    this.albumService.addComment(album, text);

    form.reset();
  }



}
