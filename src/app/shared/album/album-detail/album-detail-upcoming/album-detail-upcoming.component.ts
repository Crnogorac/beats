import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Album } from '../../album.model';
import { AlbumService } from '../../album.service';

@Component({
  selector: 'app-album-detail-upcoming',
  templateUrl: './album-detail-upcoming.component.html',
  styleUrls: ['./album-detail-upcoming.component.css']
})
export class AlbumDetailUpcomingComponent implements OnInit {

  album: Album;
  id: number;

  constructor(public albumService: AlbumService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.album = this.albumService.getOneUpcoming(this.id);
        }
      );
  }
}

