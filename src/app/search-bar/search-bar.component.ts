import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Album } from 'src/app/shared/album/album.model';
import { AlbumService } from 'src/app/shared/album/album.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit, OnDestroy {
  isAuthed = false;
  private userSub: Subscription;
  albums: Album[];
  subscription: Subscription;
  public searchText: Album['name'];
  isTyped: false;

  constructor(public albumService: AlbumService,
              private authService: AuthService) { }

  ngOnInit() {

    this.userSub = this.authService.user.subscribe(user => {
      this.isAuthed = !!user;
    });

    this.subscription = this.albumService.albumsChanged
      .subscribe(
        (albums: Album[]) => {
          this.albums = albums;
        }
      );
    this.albums = this.albumService.getAlbums();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  

}
