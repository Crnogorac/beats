import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Album } from 'src/app/shared/album/album.model';
import { AlbumService } from 'src/app/shared/album/album.service';

@Component({
  selector: 'app-upcoming-albums-expanded',
  templateUrl: './upcoming-albums-expanded.component.html',
  styleUrls: ['./upcoming-albums-expanded.component.css']
})
export class UpcomingAlbumsExpandedComponent implements OnInit {

  constructor(private albumService: AlbumService) { }
  
  albumsUpcoming: Album[];
  subscription: Subscription;


  ngOnInit() {
    this.subscription = this.albumService.albumsChanged
      .subscribe(
        (albumsUpcoming: Album[]) => {
          this.albumsUpcoming = albumsUpcoming;
        }
      );
    this.albumsUpcoming = this.albumService.getUpcomingAlbums();
  }

}
