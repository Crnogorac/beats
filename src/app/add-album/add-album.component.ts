import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AlbumService } from '../shared/album/album.service';

@Component({
  selector: 'app-add-album',
  templateUrl: './add-album.component.html',
  styleUrls: ['./add-album.component.css']
})
export class AddAlbumComponent implements OnInit {


  constructor(public albumService: AlbumService) { }

  ngOnInit() {
  }

  onSubmit(name: string, artist: string, description: string, releaseDate: Date, imgURL: string, form: NgForm, spotify?: string, youtube?: string){

    this.albumService.addAlbum(name, artist, description, releaseDate, imgURL, spotify, youtube);

    form.reset();
  }
  
}
