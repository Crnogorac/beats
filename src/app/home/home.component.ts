import { Component, OnInit } from '@angular/core';
import { Album } from '../shared/album/album.model';
import { AlbumService } from '../shared/album/album.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  

  constructor(private albumService: AlbumService) { }

  albumsRated: Album[];
  albumsUpcoming: Album[];
  albumsNewReleases: Album[];
  
  
  ngOnInit() {
    this.albumsRated = this.albumService.getHighestRatedAlbums();
    this.albumsUpcoming = this.albumService.getUpcomingAlbums();
    this.albumsNewReleases = this.albumService.getNewReleases();
  }

  

}
