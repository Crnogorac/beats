import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { Album } from '../shared/album/album.model';
import { AlbumService } from '../shared/album/album.service';
import { DataStorageService } from '../shared/data-storage.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit, OnDestroy {

  isAuthed = false;
  private userSub: Subscription;
  public searchText: Album['name'];
  subscription: Subscription;
  albums: Album[];

  constructor(private authService: AuthService,
              private dsService: DataStorageService,
              private albumService: AlbumService){}

  ngOnInit() {
    this.userSub = this.authService.user.subscribe(user => {
      this.isAuthed = !!user;
    });

    this.subscription = this.albumService.albumsChanged
      .subscribe(
        (albums: Album[]) => {
          this.albums = albums;
        }
      );
    this.albums = this.albumService.getAlbums()
    console.log(this.albums);
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }


}
