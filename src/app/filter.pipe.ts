import { Pipe, PipeTransform } from '@angular/core';
import { Album } from './shared/album/album.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: Album[], searchText: string): Album[] {

    // if (!items) {
    //   return [];
    // }
    // if (!searchText) {
    //   return items;
    // }
    // searchText = searchText.toLocaleLowerCase();

    // return items.filter(it => {
    //   return it.toLocaleLowerCase().includes(searchText);
    // });

    searchText = searchText ? searchText.toLocaleLowerCase() : null;
    return searchText ? items.filter((album: Album) =>
        album.name.toLocaleLowerCase().indexOf(searchText) !== -1) : items;

  }

  

}
