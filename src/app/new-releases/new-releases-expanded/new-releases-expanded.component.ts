import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Album } from 'src/app/shared/album/album.model';
import { AlbumService } from 'src/app/shared/album/album.service';

@Component({
  selector: 'app-new-releases-expanded',
  templateUrl: './new-releases-expanded.component.html',
  styleUrls: ['./new-releases-expanded.component.css']
})
export class NewReleasesExpandedComponent implements OnInit, OnDestroy {
  albumsNewReleases: Album[];
  subscription: Subscription;

  constructor(private albumService: AlbumService) { }
  

  ngOnInit() {
    this.subscription = this.albumService.albumsChanged
      .subscribe(
        (albumsNewReleases: Album[]) => {
          this.albumsNewReleases = albumsNewReleases;
        }
      );
    this.albumsNewReleases = this.albumService.getNewReleases();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
