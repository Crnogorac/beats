import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { AlbumListComponent } from './shared/album/album-list/album-list.component';
import { AlbumItemComponent } from './shared/album/album-list/album-item/album-item.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AlbumListExpandedComponent } from './shared/album/album-list-expanded/album-list-expanded.component';
import { AlbumItemExpandedComponent } from './shared/album/album-list-expanded/album-item-expanded/album-item-expanded.component';
import { HighestRatedComponent } from './highest-rated/highest-rated.component';
import { HighestRatedExpandedComponent } from './highest-rated/highest-rated-expanded/highest-rated-expanded.component';
import { AppRoutingModule } from './app-routing.module';
import { AlbumDetailComponent } from './shared/album/album-detail/album-detail.component';
import { AlbumService } from './shared/album/album.service';
import { HomeComponent } from './home/home.component';
import { UpcomingAlbumsComponent } from './upcoming-albums/upcoming-albums.component';
import { UpcomingAlbumsExpandedComponent } from './upcoming-albums/upcoming-albums-expanded/upcoming-albums-expanded.component';
import { NewReleasesComponent } from './new-releases/new-releases.component';
import { NewReleasesExpandedComponent } from './new-releases/new-releases-expanded/new-releases-expanded.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlbumDetailNewComponent } from './shared/album/album-detail/album-detail-new/album-detail-new.component';
import { AlbumDetailUpcomingComponent } from './shared/album/album-detail/album-detail-upcoming/album-detail-upcoming.component';
import { AlbumDetailHighestComponent } from './shared/album/album-detail/album-detail-highest/album-detail-highest.component';
import { AuthComponent } from './auth/auth.component';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { AllAlbumsComponent } from './all-albums/all-albums.component';
import { AlbumsStartComponent } from './shared/album/albums-start/albums-start.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FilterPipe } from './filter.pipe';
import { AddAlbumComponent } from './add-album/add-album.component';


@NgModule({
  declarations: [
    AppComponent,
    DropdownDirective,
    AlbumListComponent,
    AlbumItemComponent,
    HeaderComponent,
    FooterComponent,
    AlbumListExpandedComponent,
    AlbumItemExpandedComponent,
    HighestRatedComponent,
    HighestRatedExpandedComponent,
    AlbumDetailComponent,
    HomeComponent,
    UpcomingAlbumsComponent,
    UpcomingAlbumsExpandedComponent,
    NewReleasesComponent,
    NewReleasesExpandedComponent,
    AlbumDetailNewComponent,
    AlbumDetailUpcomingComponent,
    AlbumDetailHighestComponent,
    AuthComponent,
    LoadingSpinnerComponent,
    AllAlbumsComponent,
    AlbumsStartComponent,
    SearchBarComponent,
    FilterPipe,
    AddAlbumComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AlbumService, 
    {provide: 
      HTTP_INTERCEPTORS, 
      useClass: AuthInterceptorService, 
      multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
